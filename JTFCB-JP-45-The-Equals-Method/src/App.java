// sluzy do porownywania obiektow
// metoda "dot equals" tak sie na nia mowi
class Person {

	private int id;
	private String name;

	public Person(int id, String name) {
		this.id = id;
		this.name = name;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", name=" + name + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		return result;
	}

	// wygenerowane
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Person other = (Person) obj;
		if (id != other.id)
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		return true;
	}

}

public class App {

	public static void main(String[] args) {

		// pomimo ze sa skopiowane i sa takie same to i tak bedzie false
		Person person1 = new Person(5, "Bob");
		// Person person2 = new Person(8, "Sue");
		Person person2 = new Person(5, "Bob");

		// jak wpisze takie cos to bedzie true
		// person2 = person1;

		// bez tego powyzej jest false
		// System.out.println(person1 == person2);
		// dopoki nie powiemy equals jak ma porownywac bedzie false
		// generate hash code and equals w klasie prawym przyciskiem
		System.out.println(person1.equals(person2));

		// to jest false (nie wie dlaczego double tak ma)
		// lepiej nie porownywac takich liczb
		Double value1 = 7.2;
		Double value2 = 7.2;
		System.out.println(value1 == value2);

		// to jest true
		Integer number1 = 6;
		Integer number2 = 6;
		// System.out.println(number1 == number2);
		System.out.println(number1.equals(number2));

		// Stringi mozna porownywac metoda equals
		String text1 = "Hello";
		String text2 = "Hello";
		System.out.println(text1 == text2);

		// Zawsze equals dot
		String text3 = "Hello";
		String text4 = "Hellodfdaf".substring(0, 5);
		System.out.println(text4);
		System.out.println(text3.equals(text4));
	}

}
